#include <iostream>
#include <random>
#include <stdexcept>
#include <numeric>
#include <cmath>
#include <fstream>
#include "ffft/FFTReal.h"

    using namespace std;

    	std::vector<float> SigGen(double mean, double stddev, int length){
    	std::default_random_engine generator;
    	std::normal_distribution<float> distribution(mean, stddev);
    	std::vector<float> aVector(length);
    	for (int i = 0; i < length; i++){
    		float number = distribution(generator);
    		aVector[i] = number;
    	}
    		return aVector;
    };
// wprowadzenie mozliwych problemow -> oblsuga wyjatkow (try, catch)
    class Error : std::exception{ // glowny problem
    public:
    	string message;
    	Error(string message){ this -> message = message; };
    	virtual const char* what() const noexcept override{ return this -> message.c_str(); }
    };
    class zero : public Error{ //dzielenie przez zero
    public:
    	zero(string zero_m) :Error(zero_m){}
    };
    class open : public Error{ //otwarcie pliku
    public:
    	open(string zero_m) : Error(zero_m){}
    };
    class buff_size : public Error{ //rozmiar
    public:
    	buff_size(string zero_m) : Error(zero_m){}
    };

    class Packet{
    protected:
    	    protected:
    	    string device;  //nazwa urzadzenia
    	    string description;  //opis danych
    	    long date; //czas od poczatku
    public:
    	Packet(string urzadzenie, string opis, long czas) :
    		device(urzadzenie), description(opis), date(czas){}
    	~Packet(){};
    	void toString(){
    		cout << endl << "Urzadzenie: " << device << endl;
    		cout << "Opis danych: " << description << endl;
    		cout << "Czas: " << date << endl;
    		}
    };

    template <class T, int size>
    class Sequence : protected Packet{
    protected:
    	    int channelNr;  //numer kanalu
    	    string unit;  //jednostka
    	    double resolution;  //rozdzielczosc
    	    vector<T> buffer; //
    public:
    	Sequence(string urzadzenie, string opis, long czas, int kanal, string jednostka, double rozdzielczosc) :
    		Packet(urzadzenie, opis, czas), channelNr(kanal), unit(jednostka), resolution(rozdzielczosc){buffer.resize(size);}
    	~Sequence(){};
    	void toString(){
    		Packet::toString();
    		//wypisanie danych
    		cout << "Numer kanalu: " << channelNr << endl;
    		cout << "Jednostka: " << unit << endl;
    		cout << "Rozdzielczosc: " << resolution << endl;
    		}

    	template<class T1, int size1>
    	friend void operator<(string& filename, Sequence<T1, size1>& Sq);

    	template<class T1, int size1>
    	friend void operator<=(string& filename, Sequence<T1, size1>& Sq);

    	template<class T1, int size1>
    	friend void operator>(const vector<float>& Input_Data,  Sequence<T1, size1>& Sq);

    	template<class T1, int size1>
    	friend Sequence<T1, size1> operator+(const Sequence<T1, size1>& input_S1, const Sequence<T1, size1>& input_S2);

    	template<class T1, int size1>
    	friend Sequence<T1, size1> operator/(const Sequence<T1, size1>& input_S1, const Sequence<T1, size1>& input_S2);

    	template<class T1, int size1>
    	friend void operator>=(Sequence<T1, size1>& input_S1, Sequence<T1, size1>& input_S2);


    	template<class T1, int size1>
    	friend T1 RMS(Sequence<T1, size1> input_sq);
    };

    template <class T, int size>
    class TimeHistory : protected Sequence<T, size>{
    protected:
    	double sensitivity; //czulosc napieciowa
    public:
    	TimeHistory(string urzadzenie, string opis, long czas, int kanal, string jednostka, double rozdzielczosc, double czulosc) :
    		Sequence<T, size>(urzadzenie, opis, czas, kanal, jednostka, rozdzielczosc), sensitivity(czulosc){};
    	~TimeHistory(){};
    	void toString(){
    			Sequence<T, size>::toString();
    			//wypisanie danych
    			cout << "Czulosc: " << sensitivity << endl;
    		}

    };

    template <class T, int size>
    class Spectrum : protected Sequence<T, size>{
    	string scalling; //skala - liniowa/logarytmiczna
    public:
    	Spectrum(string urzadzenie, string opis, long czas, int kanal, string jednostka, double rozdzielczosc, string czulosc) :
    		Sequence<T, size>(urzadzenie, opis, czas, kanal, jednostka, rozdzielczosc), scalling(czulosc){};
    	~Spectrum(){};
    	void toString(){
    		Sequence<T, size>::toString();
    		//wypisanie danych
    		cout << "Skala: " << scalling << endl;
    	}


    };

    class Alarm : protected Packet{
    protected:

    	int channelNO; //numer kanalu
    	double treshold; //po przekroczeniu tej wartosci wlacza sie alarm
    	int direction;  //kierunek zmiany 0-dowolny, 1-w gore, -1-w dol

    public:
    	Alarm(string urzadzenie, string opis, long czas,int NrKanalu, double Wartosc, int kierunek) :
    			Packet(urzadzenie, opis, czas), channelNO(NrKanalu), treshold(Wartosc), direction(kierunek){}
    	~Alarm(){};
    	void toString(){
    		Packet::toString();
    		//wypisanie danch
    		cout << "Numer kanalu: " << channelNO << endl;
    		cout << "Wartosc: " << treshold << endl;
    		cout << "Kierunek: " << direction << endl;
    	}

    };
    	//operacje na sygnalach
    	template<class T1, int size1>
    	void operator<(string& filename, Sequence<T1, size1>& Sq){
    		ofstream outputFile;
    		outputFile.open(filename);
    		if(outputFile.is_open()){
    			outputFile << "Time(s/res)       Input" << endl;
    			for (int a = 0; a < size1; a++){
    				outputFile << a << "                 " << Sq.buffer[a] << endl << endl;
    			}
    			long len = 1024;
    			ffft::FFTReal <float> fft_object (len);
    			int L = Sq.buffer.size();
    			float x [L];
    			float f [L];
    			for (int i = 0; i < L; i++){
    				x[i] = Sq.buffer[i];
    			}
    			fft_object.do_fft(f, x);
    			outputFile << endl << "FFT:" << endl;
    			outputFile << endl << "Re: "<< "      " << "Im: " <<endl;
    			outputFile << f[0] << "       " << 0 <<endl;
    			outputFile << f[1] << "       " << f[size1/2+1] <<endl;
    			for (int i = 1; i < (size1/2-1); i++){
    				outputFile << f[i] << " 	" << f[i] <<endl;
    			}
    			outputFile << f[size1/2-1] << "       " << f[size1-1] <<endl;
    			outputFile << f[size1/2] << "       " << 0 <<endl;
    			outputFile << f[size1/2-1] << "       " << -f[size1-1] <<endl;
    			for (int i = size1/2+2; i < (size1-1); i++){
    				outputFile << f[1] << "       " << -f[i] <<endl;
    			}
    			outputFile << f[1] << "       " << -f[size1/2+1] <<endl;
    		}else{
    			throw open("Nie mozna otworzyc pliku");
    		}
    		outputFile.close();
    	};
    	// klasa odpowiedzialna za fft/rms
    	template<class T1, int size1>
    	void operator<=(string& filename, Sequence<T1, size1>& Sq){
    		ofstream outputFile;
    		outputFile.open(filename);
    		if(outputFile.is_open()){
    			outputFile << "Time(s/res)       Input" << endl;
    			for (int a = 0; a < size1; a++){
    				outputFile << a << "                 " << Sq.buffer[a] << endl << endl;
    			}
    			long len = 1024;
    			ffft::FFTReal <float> fft_object (len);
    			int L = Sq.buffer.size();
    			float x [L];
    			float f [L];
    			for (int i = 0; i < L; i++){
    					x[i] = Sq.buffer[i];
    				}
    			fft_object.do_fft(f, x);
    			outputFile << endl << "FFT:" << endl;
    			outputFile << endl << "Mod: " << endl;
    			outputFile << sqrt(pow(f[0],2) + pow(0, 2)) <<endl;
    			outputFile << sqrt(pow(f[1],2) + pow(f[size1/2+1], 2)) << endl;
    			for (int i = 1; i < (size1/2-1); i++){
    				outputFile << sqrt(pow(f[i],2) + pow(f[i], 2)) << endl;
    			}
    			outputFile << sqrt(pow(f[size1/2-1],2) + pow(f[size1-1], 2)) << endl;
    			outputFile << sqrt(pow(f[size1/2],2) + pow(0, 2)) << endl;
    			outputFile << sqrt(pow(f[size1/2-1],2) + pow(-f[size1-1], 2)) << endl;
    			for (int i = size1/2+2; i < (size1-1); i++){
    				outputFile << sqrt(pow(f[i],2) + pow(-f[i], 2)) << endl;
    			}
    			outputFile << sqrt(pow(f[1],2) + pow(-f[size1/2+1], 2)) << endl;
    		}
    		else{
    					throw open("Nie mozna otworzyc pliku");
    		}
    		outputFile.close();
    	}

    	template<class T1, int size1>
    	void operator>(const vector<float>& Input_Data,  Sequence<T1, size1>& Sq){
    		int S = Input_Data.size();
    		for(int count = 0; count < S; count ++){
    		Sq.buffer[count] = Input_Data[count];
    		}
    	}

    	template<class T1, int size1>
    	Sequence<T1, size1> operator+(const Sequence<T1, size1>& input_S1, const Sequence<T1, size1>& input_S2){
    		Sequence<T1, size1> out("Suma", "Suma sygnalow wejsciowych", input_S1.date, 0, input_S1.unit, input_S1.resolution);
    		for(int i = 0; i < size1; i++){
    			out.buffer[i] = input_S1.buffer[i] + input_S2.buffer[i];
    		}
    		return out;
    	}
    	template<class T1, int size1>
    	Sequence<T1, size1> operator/(const Sequence<T1, size1>& input_S1, const Sequence<T1, size1>& input_S2){
    		Sequence<T1, size1> out("Dzielenie", "Dzielenie sygnalow wejsciowych", input_S1.date, 0, input_S1.unit, input_S1.resolution);
    		for(int i = 0; i < size1; i++){
    			if(input_S2.buffer[i] == 0){
    				throw zero("Dzielenie przez 0");
    			}
    			out.buffer[i] = input_S1.buffer[i] / input_S2.buffer[i];
    		}
    		return out;
    	}

    	template<class T1, int size1>
    	void operator>=(Sequence<T1, size1>& input_S1, Sequence<T1, size1>& input_S2){
    		vector<T1>swap_buffer(size1);
    		if(input_S1.buffer.size() == input_S2.buffer.size()){
    			for(int i = 0; i < size1; i++){
    				swap_buffer[i] = input_S1.buffer[i];
    				input_S1.buffer[i] = input_S2.buffer[i];
    				input_S2.buffer[i] = swap_buffer[i];
    			}
    		}else{
    			throw buff_size("Zly rozmiar");
    		}
    	}

    template<class T1, int size1>
    T1 RMS(Sequence<T1,size1> input_sq){
    	int N = size1;
    	double xui_sum = 0;
    	double xui = 0;
    	double sum = std::accumulate(input_sq.buffer.begin(), input_sq.buffer.end(), 0.0);
    	double ui = sum / N;
    	for(int i = 0; i < N; i++){
    		double xui_p1 = input_sq.buffer[i] - ui;
    		xui = pow(xui_p1, 2.0);
    		xui_sum = xui_sum + xui;
    	}
    	double sig_p2 = (xui_sum/(N-1));
    	double sig = sqrt(sig_p2);
    	cout << endl << input_sq.description << endl;
    	cout << "RMS = " << sig << endl; //RMS
    	return sig;
    };

    int main(){
    try{

    	double time = 10; // czas probki
    	double res = 400; // czestotliwosc [Hz]
    	int length = time * res;
    	long dat = 2000;
    	string unit = "V"; // jednostka
    	string scalling = "log"; //typ skalowania - log/liniowa

    	// zdefiniowanie plikow txt (gdzie sa zapisywane)
    	string Kanal_wy_1 = "Kanal1.txt";
    	string Kanal_wy_2 = "Kanal2.txt";
    	string Kanal_wy_3 = "Kanal3.txt";
    	string Kanal_wy_4 = "Kanal4.txt";
    	string Wyjsciowa_suma = "Suma_kanalow.txt";
    	string Iloraz = "Dzielenie_kanalow.txt";
    	string Wyjsciowa_zamiana = "Zamiana_kanalow.txt";
    	string Widmo_amp_1 = "Kanal1_widmo_amp.txt";
    	string Widmo_amp_2 = "Kanal2_widmo_amp.txt";
    	string Widmo_amp_3 = "Kanal3_widmo_amp.txt";
    	string Widmo_amp_4 = "Kanal4_widmo_amp.txt";

    //losowe sygnaly (bialy szum) - dane z tabelki
    	vector<float> Signal_1(length);
    	vector<float> Signal_2(length);
    	vector<float> Signal_3(length);
    	vector<float> Signal_4(length);
    	for(int i = 0; i < length; i++){
    		Signal_1[i] = SigGen(3, 0.3, length)[i];
    		Signal_2[i] = SigGen(6, 0.3, length)[i];
    		Signal_3[i] = SigGen(8, 0.1, length)[i];
    		Signal_4[i] = SigGen(3, 0.3, length)[i];
    	}


    	//inicjalizacja sekwencji rozmiar bufora
    	Sequence<float, 4000> S1("Czujnik 1","Sen 1 Kanal 1", dat, 1, unit, res);
    	Sequence<float, 4000> S2("Czujnik 1","Sen 2 Kanal 2", dat, 2, unit, res);
    	Sequence<float, 4000> S3("Czujnik 1","Sen 3 Kanal 3", dat, 1, unit, res);
    	Sequence<float, 4000> S4("Czujnik 1","Sen 4 Kanal 4", dat, 2, unit, res);
    	Spectrum<int, 5> x("Czujnik 2","2 czujnik", dat, 1, unit, res, scalling);
    	TimeHistory<int, 5> y("Czujnik 3","3 czujnik", 21500, 3, "km/s", 33, 0.01);
    	Alarm z("Czujnik milion","Milionowy czujnik", 14150, 0, 0.5, -5);

//    	wypisanie pierwszej czesci zadania
    	x.toString();
    	y.toString();
    	z.toString();

    	//Inicjacja oraz zapis danych
    	Signal_1 > S1;
    	Signal_2 > S2;
    	Signal_3 > S3;
    	Signal_4 > S4;

    	//zamiana kanalow
    	Signal_1 >= Signal_2;
    	Sequence<float, 4000> sum2_3 = S2 + S3;
    	Wyjsciowa_suma < sum2_3;
    	Sequence<float, 4000> div1_4 = S1 / S4;
    	Iloraz < div1_4;

    	// FFT, zapis do pliku txt
    	Kanal_wy_1 < S1;
    	Kanal_wy_2 < S2;
    	Kanal_wy_3 < S3;
    	Kanal_wy_4 < S4;
    	Widmo_amp_1 <= S1;
    	RMS(S1);
    	RMS(S2);
    	RMS(S3);
    	RMS(S4);

    } catch(zero&){
    	cerr << "Dzielenie przez 0" << endl;
    } catch(open&){
    	cerr << "Nie mo�na otworzyc" << endl;
    } catch(buff_size&){
    	cerr << "Zly rozmiar" << endl;
    }
    return 0;

    }
